[Install link for desktop Chrome or mobile Yandex.Browser + Tampermonkey](https://gitlab.com/ruslan.levitskiy/NotifyOPR/raw/master/NotifyOPR.user.js)

On Android devices, you need to install [Yandex.Browser](https://play.google.com/store/apps/details?id=com.yandex.browser) and [Tampermonkey Chrome add-on](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo). After that, paste the userscript URL `https://gitlab.com/ruslan.levitskiy/NotifyOPR/raw/master/NotifyOPR.user.js` into the Yandex.Browser address bar (or click install link above) and confirm the installation.

___

###### Please support the original developer. Read more: [https://ruslan-levitskiy.tk/](https://ruslan-levitskiy.tk/?utm_source=gitlab&utm_medium=notify)